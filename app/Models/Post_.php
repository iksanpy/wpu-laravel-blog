<?php

namespace App\Models;

class Post
{
    private static $blog_posts = [
        [
            "tittle" => "Judul satu",
            "slug" => "judul-satu",
            "author" => "Iksan",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum perferendis esse blanditiis illo quibusdam facilis similique, dolorem recusandae ut amet soluta doloribus obcaecati eius assumenda sapiente totam aspernatur temporibus neque facere delectus consequuntur velit earum. Inventore deleniti praesentium id molestiae voluptatem error, ut magnam quos! Iure temporibus soluta nemo dolorem deleniti itaque perferendis qui libero! Necessitatibus saepe provident illum quae rem dolorem aperiam perspiciatis odit similique impedit, quidem aut voluptatibus facere magnam, dolores facilis natus. Optio quam quis molestiae voluptatem."
        ], [
            "tittle" => "Judul dua",
            "slug" => "judul-dua",
            "author" => "Iksan",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum perferendis esse blanditiis illo quibusdam facilis similique, dolorem recusandae ut amet soluta doloribus obcaecati eius assumenda sapiente totam aspernatur temporibus neque facere delectus consequuntur velit earum. Inventore deleniti praesentium id molestiae voluptatem error, ut magnam quos! Iure temporibus soluta nemo dolorem deleniti itaque perferendis qui libero! Necessitatibus saepe provident illum quae rem dolorem aperiam perspiciatis odit similique impedit, quidem aut voluptatibus facere magnam, dolores facilis natus. Optio quam quis molestiae voluptatem. soluta nemo dolorem deleniti itaque perferendis qui libero! Necessitatibus saepe provident illum quae rem dolorem aperiam perspiciatis odit similique impedit, quidem aut voluptatibus facere magnam, dolores facilis natus. Optio quam quis molestiae voluptatem."
        ]
    ];

    public static function all()
    {
        return collect(self::$blog_posts);
    }

    public static function find($slug)
    {
        $blog_posts = static::all();
        // $new_post = [];
        // foreach ($blog_posts as $post) :
        //     if ($post['slug'] === $slug) :
        //         $new_post = $post;
        //     endif;
        // endforeach;

        return $blog_posts->firstWhere('slug', $slug);
    }
}
