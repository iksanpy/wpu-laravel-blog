<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(5)->create();

        // User::create([
        //     'name' => 'Ahmad Iksan',
        //     'email' => 'iksan@gmail.com',
        //     'password' => Hash::make('12345')
        // ]);

        // User::create([
        //     'name' => 'Sandika Galih',
        //     'email' => 'sandika@gmail.com',
        //     'password' => Hash::make('12345')
        // ]);

        // Category::factory(5)->create();

        Category::create([
            'name' => 'Web programming',
            'slug' => 'web-programming'
        ]);

        Category::create([
            'name' => 'Web design',
            'slug' => 'web-design'
        ]);

        Category::create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);

        Post::factory(20)->create();

        // Post::create([
        //     'tittle' => 'Judul Pertama',
        //     'slug' => 'judul-pertama',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic temporibus expedita libero sed illum nemo?',
        //     'body' => '<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Commodi totam, sequi praesentium voluptas doloremque quidem in, quisquam tempora quos nemo consectetur facere recusandae neque pariatur fugit magni deserunt numquam magnam mollitia. Cupiditate dolores numquam architecto provident velit repudiandae corrupti sequi eveniet, aperiam fugiat saepe consequuntur praesentium ullam suscipit! Veniam tenetur corrupti quis magnam maiores? Ullam rem praesentium dolore provident</p><p> nesciunt excepturi repellat voluptatem odio itaque. Quod soluta ratione odit ab possimus repellat iure sapiente aut nulla rem. Consequatur ex, fugiat, magnam expedita hic deleniti vitae aliquam animi facilis sed atque velit eius quae illum, rem optio sit harum deserunt maiores debitis eligendi reiciendis! Temporibus, porro. Laborum mollitia tenetur beatae blanditiis repudiandae dicta soluta earum, praesentium error laboriosam ab minus nemo quidem deserunt dolor eveniet commodi quas facilis, recusandae,</p><p>odit reprehenderit quo! Laboriosam, ducimus? Reiciendis magnam eaque ducimus id et consectetur odit iure consequatur, obcaecati aperiam soluta rerum quibusdam, placeat accusantium ad quod quo ipsam. Sunt eius perferendis ipsa non labore nostrum unde nesciunt, sed cum alias, magni et est deserunt vel nihil praesentium quis officiis mollitia veritatis id magnam, in voluptatibus repellat tempora. Architecto nihil corrupti eveniet! Dolorum repellat ut quo, facilis obcaecati ad dicta quia explicabo aliquid itaque quidem.</p>',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);

        // Post::create([
        //     'tittle' => 'Judul Ke Dua',
        //     'slug' => 'judul-ke-dua',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic temporibus expedita libero sed illum nemo?',
        //     'body' => '<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Commodi totam, sequi praesentium voluptas doloremque quidem in, quisquam tempora quos nemo consectetur facere recusandae neque pariatur fugit magni deserunt numquam magnam mollitia. Cupiditate dolores numquam architecto provident velit repudiandae corrupti sequi eveniet, aperiam fugiat saepe consequuntur praesentium ullam suscipit! Veniam tenetur corrupti quis magnam maiores? Ullam rem praesentium dolore provident</p><p> nesciunt excepturi repellat voluptatem odio itaque. Quod soluta ratione odit ab possimus repellat iure sapiente aut nulla rem. Consequatur ex, fugiat, magnam expedita hic deleniti vitae aliquam animi facilis sed atque velit eius quae illum, rem optio sit harum deserunt maiores debitis eligendi reiciendis! Temporibus, porro. Laborum mollitia tenetur beatae blanditiis repudiandae dicta soluta earum, praesentium error laboriosam ab minus nemo quidem deserunt dolor eveniet commodi quas facilis, recusandae,</p><p>odit reprehenderit quo! Laboriosam, ducimus? Reiciendis magnam eaque ducimus id et consectetur odit iure consequatur, obcaecati aperiam soluta rerum quibusdam, placeat accusantium ad quod quo ipsam. Sunt eius perferendis ipsa non labore nostrum unde nesciunt, sed cum alias, magni et est deserunt vel nihil praesentium quis officiis mollitia veritatis id magnam, in voluptatibus repellat tempora. Architecto nihil corrupti eveniet! Dolorum repellat ut quo, facilis obcaecati ad dicta quia explicabo aliquid itaque quidem.</p>',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);

        // Post::create([
        //     'tittle' => 'Judul Ke Tiga',
        //     'slug' => 'judul-ke-tiga',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic temporibus expedita libero sed illum nemo?',
        //     'body' => '<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Commodi totam, sequi praesentium voluptas doloremque quidem in, quisquam tempora quos nemo consectetur facere recusandae neque pariatur fugit magni deserunt numquam magnam mollitia. Cupiditate dolores numquam architecto provident velit repudiandae corrupti sequi eveniet, aperiam fugiat saepe consequuntur praesentium ullam suscipit! Veniam tenetur corrupti quis magnam maiores? Ullam rem praesentium dolore provident</p><p> nesciunt excepturi repellat voluptatem odio itaque. Quod soluta ratione odit ab possimus repellat iure sapiente aut nulla rem. Consequatur ex, fugiat, magnam expedita hic deleniti vitae aliquam animi facilis sed atque velit eius quae illum, rem optio sit harum deserunt maiores debitis eligendi reiciendis! Temporibus, porro. Laborum mollitia tenetur beatae blanditiis repudiandae dicta soluta earum, praesentium error laboriosam ab minus nemo quidem deserunt dolor eveniet commodi quas facilis, recusandae,</p><p>odit reprehenderit quo! Laboriosam, ducimus? Reiciendis magnam eaque ducimus id et consectetur odit iure consequatur, obcaecati aperiam soluta rerum quibusdam, placeat accusantium ad quod quo ipsam. Sunt eius perferendis ipsa non labore nostrum unde nesciunt, sed cum alias, magni et est deserunt vel nihil praesentium quis officiis mollitia veritatis id magnam, in voluptatibus repellat tempora. Architecto nihil corrupti eveniet! Dolorum repellat ut quo, facilis obcaecati ad dicta quia explicabo aliquid itaque quidem.</p>',
        //     'category_id' => 2,
        //     'user_id' => 1
        // ]);

        // Post::create([
        //     'tittle' => 'Judul Ke Empat',
        //     'slug' => 'judul-ke-empat',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic temporibus expedita libero sed illum nemo?',
        //     'body' => '<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Commodi totam, sequi praesentium voluptas doloremque quidem in, quisquam tempora quos nemo consectetur facere recusandae neque pariatur fugit magni deserunt numquam magnam mollitia. Cupiditate dolores numquam architecto provident velit repudiandae corrupti sequi eveniet, aperiam fugiat saepe consequuntur praesentium ullam suscipit! Veniam tenetur corrupti quis magnam maiores? Ullam rem praesentium dolore provident</p><p> nesciunt excepturi repellat voluptatem odio itaque. Quod soluta ratione odit ab possimus repellat iure sapiente aut nulla rem. Consequatur ex, fugiat, magnam expedita hic deleniti vitae aliquam animi facilis sed atque velit eius quae illum, rem optio sit harum deserunt maiores debitis eligendi reiciendis! Temporibus, porro. Laborum mollitia tenetur beatae blanditiis repudiandae dicta soluta earum, praesentium error laboriosam ab minus nemo quidem deserunt dolor eveniet commodi quas facilis, recusandae,</p><p>odit reprehenderit quo! Laboriosam, ducimus? Reiciendis magnam eaque ducimus id et consectetur odit iure consequatur, obcaecati aperiam soluta rerum quibusdam, placeat accusantium ad quod quo ipsam. Sunt eius perferendis ipsa non labore nostrum unde nesciunt, sed cum alias, magni et est deserunt vel nihil praesentium quis officiis mollitia veritatis id magnam, in voluptatibus repellat tempora. Architecto nihil corrupti eveniet! Dolorum repellat ut quo, facilis obcaecati ad dicta quia explicabo aliquid itaque quidem.</p>',
        //     'category_id' => 2,
        //     'user_id' => 2
        // ]);
    }
}
