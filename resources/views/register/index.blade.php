@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <main class="form-registration">
                <h1 class="h3 mb-3 fw-normal text-center ">Registration Form</h1>
                {{-- @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif --}}
                <form action="/register" method="post">
                    @csrf
                    <div class="form-floating">
                        <input type="text" name="name" value="{{ old('name') }}"
                            class="form-control rounded-top @error('name') is-invalid @enderror" required
                            placeholder="Andreas">
                        <label for="floatingInput">Name</label>
                        @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-floating">
                        <input type="text" name="username" class="form-control  @error('username') is-invalid @enderror"
                            value="{{ old('username') }}" required placeholder="andreas267">
                        <label for=" floatingInput">Username</label>
                        @error('username')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-floating">
                        <input type="email" name="email" class="form-control  @error('email') is-invalid @enderror"
                            value="{{ old('email') }}" required placeholder="name@example.com">
                        <label for="floatingInput">Email address</label>
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-floating mb-2">
                        <input type="password" name="password"
                            class="form-control  @error('password') is-invalid @enderror  rounded-bottom"
                            id="floatingPassword" required placeholder="Password">
                        <label for="floatingPassword">Password</label>
                        @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button class="w-100 btn btn-lg btn-primary" type="submit">Register</button>
                </form>
                <div class="text-center mt-3 mb-3">
                    <small class="">Have an account ? <a href="/login">Login now!</a></small>
                </div>
            </main>
        </div>
    </div>
@endsection
