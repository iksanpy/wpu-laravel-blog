@extends('dashboard.layouts.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">My Posts</h1>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show  col-lg-9   " role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <a href="/dashboard/posts/create" class="btn btn-sm btn-primary my-2"><span data-feather="plus"></span> Add new post</a>
    <div class="table-responsive col-lg-9">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tittle</th>
                    <th scope="col">Category</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @if ($posts->count() <= 1)
                    <tr>
                        <td colspan="4" class="text-center">No posts</td>
                    </tr>
                @else
                    @foreach ($posts as $post)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->category->name }}</td>
                            <td>
                                <a href="/dashboard/posts/{{ $post->slug }}" class="badge bg-info">
                                    <span data-feather="eye"></span></a>
                                <a href="/dashboard/posts/{{ $post->slug }}/edit" class="badge bg-warning">
                                    <span data-feather="edit"></span></a>
                                <form action="/dashboard/posts/{{ $post->slug }}" method="POST" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" onclick="return confirm('Are you sure?')"
                                        class="border-0 badge bg-danger"><span data-feather="x-circle"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@endsection
