@extends('layouts.main')

@section('content')

    <h1 class="mb-5">Categories</h1>

    <div class="row">
        @foreach ($categories as $category)
            <div class="col-md-4">
                <a href="/posts?category={{ $category->slug }}">
                    <div class="card bg-dark text-white">
                        <img src="https://source.unsplash.com/400x200/?{{ $category->name }}" class="card-img"
                            alt="...">
                        <div class="card-img-overlay p-0 d-flex align-items-center">
                            <h5 class="card-title text-center py-2 flex-fill" style="background-color: rgba(0, 0, 0, 0.6)">
                                {{ $category->name }}</h5>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection
